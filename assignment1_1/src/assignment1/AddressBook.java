package assignment1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class AddressBook {

	private String fullName = "";
	private String address = "";
	private String phone = "";
	private String email = "";
	private String zipcode = "";
	
	private final String SEPARATOR = ",   ";

	private List <Address> addressesList;
	// file path
	private final String URI = "C:\\Users\\sassif\\Documents\\Work_docs\\JEEJST\\addressbook.csv";	

	public AddressBook(){
		addressesList = new LinkedList<Address>();		
		displayPeopleListFromFile(new File(URI));
	}
	
	public void addAddressToList(Address addr){
		addressesList.add(addr);
	}
	
	public boolean addAddress(){
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				
		System.out.println("Enter the address details\n");
		
		System.out.println("Name: ");			
		try {
			fullName = br.readLine();
	      } 
		catch (IOException ioe) {
	         System.out.println("IO error trying to read your name!");
	         System.exit(1);
	      }
		System.out.println("Address: ");	
		try {
			address = br.readLine();
	      } 
		catch (IOException ioe) {
	         System.out.println("IO error trying to read your address!");
	         System.exit(1);
	      }				
		System.out.println("Phone: ");	
		try {
			phone = br.readLine();
	      } 
		catch (IOException ioe) {
	         System.out.println("IO error trying to read your phone!");
	         System.exit(1);
	      }			
		System.out.println("E-mail: ");	
		try {
			email = br.readLine();
	      } 
		catch (IOException ioe) {
	         System.out.println("IO error trying to read your email!");
	         System.exit(1);
	      }				
		System.out.println("Zip code: ");	
		try {
			zipcode = br.readLine();
	      } 
		catch (IOException ioe) {
	         System.out.println("IO error trying to read your zipcode!");
	         System.exit(1);
	      }
			
		Address p = new Address(fullName, address, phone, email, zipcode);
		this.addAddressToList(p);
		
		System.out.println("\nContact added.");
		
		// every catch statement exits the program so if we arrive here... the contact has been added
		return true;
	}

	public boolean deleteAddress()
	{
		String userChoiceDelete = getUserInputResults("Delete");
		
		if (getAddressByContactName(userChoiceDelete) != null) 
		{
			addressesList.remove(getAddressByContactName(userChoiceDelete));
			System.out.println("\nContact deleted.");
			return true;
		}
		else 
		{
			System.out.println("The enetered contact does not exist");
		}
		
		return false;
	}

	// add the moment prompts for all the details but the user's name
	// future implementation may prompt the user also for what field in particular he wants to edit
	public boolean editAddress(){
		
		boolean personEdited = false;
		
		String userChoiceEdit = getUserInputResults("Edit");
		
		if (this.getAddressByContactName(userChoiceEdit) != null) 
		{
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			
			System.out.println("Edit " + userChoiceEdit +  "'s details \n");
			
			System.out.println("Address: ");	
			try {
				address = br.readLine();
		      } 
			catch (IOException ioe) {
		         System.out.println("IO error trying to read your address!");
		         System.exit(1);
		      }					
			System.out.println("Phone: ");	
			try {
				phone = br.readLine();
		      } 
			catch (IOException ioe) {
		         System.out.println("IO error trying to read your phone!");
		         System.exit(1);
		      }					
			System.out.println("E-mail: ");	
			try {
				email = br.readLine();
		      } 
			catch (IOException ioe) {
		         System.out.println("IO error trying to read your email!");
		         System.exit(1);
		      }					
			System.out.println("Zip code: ");	
			try {
				zipcode = br.readLine();
		      } 
			catch (IOException ioe) {
		         System.out.println("IO error trying to read your zipcode!");
		         System.exit(1);
		      }			
			Address p = new Address(userChoiceEdit, address, phone, email, zipcode);
			
			addressesList.remove(getAddressByContactName(p.getFullName()));
			this.addAddressToList(p);
			
			System.out.println("\nContact edited.");
			
			// every catch statement exits the program so if we arrive here... the contact has been edited
			personEdited = true;
		}
		return personEdited;
	}
	
	// argument: the String corresponding to the user's name
	// the method checks each of the address and returns if the user has been find
	private Address getAddressByContactName(String name)
	{
		for (Address a : addressesList)
		{
			if (a.getFullName().equals(name)) return a;
		}
		return null;
	}

	
	public boolean sort(){
		String userChoiceSort = getUserInputResults("Sort");
		
		Collections.sort(addressesList,
               new Comparator<Address>()
               {
                    public int compare(Address f1, Address f2)
                    {
                    	if (userChoiceSort.contentEquals("Name")) 
	                    {
		                   	 // compare the last names, if they are equal...
		                     if (f1.getLastName().compareToIgnoreCase(f2.getLastName()) == 0)
		                          // compare the first names
		                          return f1.getFirstName().compareToIgnoreCase(f2.getFirstName());
	                         else return f1.getLastName().compareToIgnoreCase(f2.getLastName());
                    	 }
                    	 else if (userChoiceSort.contentEquals("Code"))	
                    	 {
                         	// if the post codes are NOT the same, compare them
                             if (f1.getZip().compareTo(f2.getZip()) != 0)
                             	return f1.getZip().compareTo(f2.getZip());  	
                             else	
                             {
                             	// otherwise compare the last names, if they are equal...
                                 if (f1.getLastName().compareToIgnoreCase (f2.getLastName()) == 0)
     	                           	 // compare the first names
     	                           	 return f1.getFirstName().compareToIgnoreCase (f2.getFirstName());
                                 else return f1.getLastName().compareToIgnoreCase (f2.getLastName());
                             }
                    	 }
                    	 return -1; // never called, for compiler only
                     }        
                 });
		   displayPeopleList();
		   return true; // for consistency with the other method calls
	}
	

	public boolean addToFile(){	
		
		try
		{		
			// any time we save, the file is cleared
			FileWriter writer = new FileWriter(URI, false);
			writer.flush();
		    writer.close();
		}
		catch(IOException ioe)
		{
			System.out.println(ioe.getMessage());
	        System.exit(1);
		} 
		
							
		for (Address s : addressesList)
		{
	
			try
			{			
				FileWriter writer = new FileWriter(URI, true);
					
				writer.append(s.getFullName().replaceAll(SEPARATOR, ","));
			    writer.append(",");
			    writer.append(s.getAddress().replaceAll(SEPARATOR, ","));
			    writer.append(",");
			    writer.append(s.getPhone().replaceAll(SEPARATOR, ","));
			    writer.append(",");
			    writer.append(s.getEmail().replaceAll(SEPARATOR, ","));
			    writer.append(",");
			    writer.append(s.getZip().replaceAll(SEPARATOR, ","));
			    writer.write(System.lineSeparator());
					
			    writer.flush();
			    writer.close();
			}
			catch(IOException e)
			{
			     e.printStackTrace();
			     return false;
			} 
		}
		return true;
	}
	
	
	public void displayPeopleList()
	{
		for (Address a : addressesList)
		{
			System.out.println(a.getFullName() + SEPARATOR + a.getAddress() + SEPARATOR + a.getPhone() 
									+ SEPARATOR + a.getEmail() + SEPARATOR + a.getZip());
		}
	}
	
	public boolean displayPeopleListFromFile(File f)
	{
		if(f.exists() && !f.isDirectory()) 
		{
		    try {

		        Scanner sc = new Scanner(f);

		        while (sc.hasNextLine()) {
		        	String line = sc.nextLine();
		        	// the array holds all the values separated by commas
		        	String[] personDetails = line.split(",");
		        	// they are the parameter for the Person constructor
		        	Address toBeAdded = new Address(
		        			personDetails[0], 
		        			personDetails[1], 
		        			personDetails[2], 
		        			personDetails[3], 
		        			personDetails[4]); 
		        	// add to the list
		        	this.addAddressToList(toBeAdded);
		        	
		        	String displayContact = 
		        			personDetails[0] + SEPARATOR +
		        			personDetails[1] + SEPARATOR +
		        			personDetails[2] + SEPARATOR +
		        			personDetails[3] + SEPARATOR +
		        			personDetails[4];
		            System.out.println(displayContact);
		        }
		        sc.close();
		        
		        return true;
		    } 
		    catch (FileNotFoundException e) {
		        e.printStackTrace();
		        return false;
		    }
		}
		return false;
	}
	
	private String getUserInputResults(String choice)
	{	
		if (choice.equals("Edit") || choice.equals("Delete"))
			System.out.println("\nEnter the name of the contact you want to delete from the address list.");
		else if (choice.equals("Sort"))
			System.out.println("\nEnter Name if you want to sort by name or Code if you want to sort by Postal Code.");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String enteredConatc = "";         
		
		try {
			enteredConatc = br.readLine().toString();
	      } 
		catch (IOException ioe) {
	         System.out.println(ioe.getMessage());
	         System.exit(1);
	      }
		return enteredConatc;
	}
}
