package assignment1;

public class Address {
	private String fullName;
	private String address;
	private String phone;
	private String email;
	private String zip;
	
	/*
	 * Constructor
	 */
	public Address(String fullN, String addr, String ph, String em, String zp){
		fullName = fullN;
		address = addr;
		phone = ph;
		email = em;
		zip = zp;
	}
	
	public String getFullName()
	{
		return fullName;
	}
	
	public String getFirstName()
	{	
		if(fullName.contains(" "))
		{
			String arr[] = fullName.split(" ", 2);
			return arr[0];
		}
		else return "";
	}
		
	public String getLastName()
	{
		if(fullName.contains(" "))
		{
			String arr[] = fullName.split(" ", 2);
			return arr[1];
		}
		else return fullName;
	}
	
	public String getAddress()
	{
		return address;
	}
	
	public void setAddress(String addr)
	{
		address = addr;
	}
	
	public String getPhone()
	{
		return phone;
	}
	
	public void setPhone(String ph)
	{
		phone = ph;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	public void setEmail(String em)
	{
		email = em;
	}
	
	public String getZip()
	{
		return zip;
	}
	
	public void setZip(String zp)
	{
		zip = zp;
	}
}
