package assignment1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	public static void main(String[] args) {
		
		boolean somethingBeenChanged = false;
		String userChoice = "";
		
		// display introductory message
		System.out.println("Welcome to address book 2.0");
		System.out.println("These are your stored contacts: \n");
		
		// start the address book
		AddressBook ab = new AddressBook();
		
		// ask the user what he wants to do
		userChoice = promptTheUserForSelection();
				
		while (userChoice.equals("Add") || userChoice.equals("Edit") || userChoice.equals("Delete") || userChoice.equals("Sort"))
		{
			switch (userChoice)
			{
				case("Add") :
					if (ab.addAddress()) 	somethingBeenChanged = true;
					break;
				case("Delete") :
					if (ab.deleteAddress()) somethingBeenChanged = true;
					break;
				case("Edit") :
					if (ab.editAddress()) 	somethingBeenChanged = true;
					break;
				case("Sort") :
					if (ab.sort())			somethingBeenChanged = true;
					break;
			}
			
			// ask again user what he wants to do
			userChoice = promptTheUserForSelection();
		}
		
		// if something has been changed: ask the user to save
		if (somethingBeenChanged == true) 
		{
			System.out.println("\nPress Save if you want to save your changes on the file");
			
			String userSave = getUserSelection();
			
			if (userSave.equals("Save")) {
				if (ab.addToFile()) System.out.println("\nChange saved.");
			}
			else System.out.println("Changes not saved.");
		}
		
		// exit the program 
		System.out.println("Application exited.");
		System.exit(0);;
	}
	
	// prompt the user to choose an action
	private static String promptTheUserForSelection()
	{
		System.out.println("\nEnter Add for adding a new contact");
		System.out.println("Enter Edit for editing a contact");
		System.out.println("Enter Delete for deleting a contact");
		System.out.println("Enter Sort for sorting your list");
		System.out.println("Enter anything else for exit the application.");
		
		return getUserSelection();
	}
	
	// return the user input from the console 
	private static String getUserSelection()
	{
		String toBeRet = "";
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			toBeRet = br.readLine().toString();
	      } 
		catch (IOException ioe) {
	         System.out.println(ioe.toString());
	         System.exit(1);
	      }
		
		return toBeRet;
	}
}
